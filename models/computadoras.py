from sqlalchemy import Column, Integer, String
from config.database import Base

class Computadora(Base):

    __tablename__ = "Computadoras"
    
    id = Column(Integer, primary_key=True)
    marca = Column(String)  # Se corrigió el nombre de la columna a minúsculas
    modelo = Column(String)
    color = Column(String)
    ram = Column(String)
    almacenamiento = Column(String)
