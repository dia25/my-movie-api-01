from fastapi import Path, Query, Depends, APIRouter
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.computadoras import Computadora as ComputadoraModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from fastapi import Request, HTTPException
from services.computadora import ComputadoraService


computadora_router = APIRouter()

class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(max_length=15)
    modelo: str = Field(min_length=5)
    color: str = Field(max_length=15, min_length=2)
    ram: str = Field(min_length=3)
    almacenamiento: str = Field(max_length=10)

#mostrar la lista de computadoras 

@computadora_router.get('/computadoras', tags=['computadoras'], response_model=List[Computadora], status_code=200, dependencies=[Depends(JWTBearer())])
def get_computadoras() -> List[Computadora]:
    db = Session()
    result = ComputadoraService(db).get_computadoras()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


#mostrar las omputadoras por id 
@computadora_router.get('/computadoras/{id}', tags=['computadoras'], dependencies=[Depends(JWTBearer())])
def get_computadora_by_id(id: int = Path(..., ge=1, le=2000)):
    db = Session()
    result = ComputadoraService(db).get_computadora_by_id(id)
    db.close()
    if not result:
        raise HTTPException(status_code=404, detail="No encontrado")
    return result

# mostrar la lista de computadoras por marca
@computadora_router.get('/computadoras/marcas/{marca}', tags=['computadoras'], dependencies=[Depends(JWTBearer())])
def get_computadoras_by_marca(marca: str = Path(..., min_length=5, max_length=15)):
    db = Session()
    data = ComputadoraService(db).get_computadoras_by_marca(marca)
    return data

#crear computadora
@computadora_router.post('/computadoras', tags=['computadoras'], dependencies=[Depends(JWTBearer())])
def create_computadora(computadora: Computadora):
    db = Session()
    new_computadora = ComputadoraModel(**computadora.dict())
    db.add(new_computadora)
    db.commit()
    db.close()
    return JSONResponse(status_code=201, content={"message": "Se ha registrado la computadora"})

# borrar la computadora
@computadora_router.delete('/computadoras/{id}', tags=['computadoras'], response_model=List[Computadora], 
            status_code=200, dependencies=[Depends(JWTBearer())])
def delete_computadora(id: int)-> dict:
    db = Session()
    reslut =db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    if not reslut:
        return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"})
    ComputadoraService(db).delate_computadora(id)
    return JSONResponse(status_code=200, content={"message":"Se ha Elimiado la computaodra"})


@computadora_router.put('/computadoras/{id}', tags=['computadoras'], response_model=Computadora, status_code=200, 
         dependencies=[Depends(JWTBearer())])
def update_computadora(id: int, computadora: Computadora)->dict:
    db = Session()
    result = ComputadoraModel(db).get_computadora(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    ComputadoraService(db).update_computadora(id,computadora)
    return JSONResponse(status_code=200, content={'Message': 'Se ha modificado la computadora'})



