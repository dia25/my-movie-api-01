from pydantic import BaseModel, Field
from typing import Optional


class Computadora(BaseModel):
    id: Optional[int] = None
    marca: str = Field(max_length=15)
    modelo: str = Field(min_length=5)
    color: str = Field(max_length=15, min_length=2)
    ram: str = Field(min_length=3)
    almacenamiento: str = Field(max_length=10)


    class Config:
        schema_extra = {
            "example": {
                "id": 1,
                "marca": "Mi Computadora",
                "modelo": "Modelo de la Computadora",
                "color": "Color de la Computadora",
                "ram": "RAM de la Computadora",
                "almacenamiento": "Almacenamiento de la Computadora"
            }
        }
