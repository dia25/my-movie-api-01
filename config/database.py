import os
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Nombre del archivo de la base de datos SQLite
sqlite_file_name = "database.sqlite"

# Directorio base del proyecto
base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# Ruta completa del archivo de la base de datos SQLite
database_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

# Crear el motor de la base de datos SQLite
engine = create_engine(database_url, echo=True)

# Crear una sesión de base de datos
Session = sessionmaker(bind=engine)

# Crear una clase base para las definiciones de las clases de la base de datos
Base = declarative_base()
