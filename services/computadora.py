from models.computadoras import Computadora as ComputadoraModel
from schemas.computadora import Computadora
class ComputadoraService():
    def __init__(self, db):
        self.db = db

    def get_computadoras(self):
        result = self.db.query(ComputadoraModel).all()
        return result
    
    def get_computadora_by_id(self, id): 
        result = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()

    def get_computadoras_by_marca(self, marca):
        reslut = self.db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()

    def create_computadora(self, computadora:Computadora):
        new_computadora = ComputadoraModel(**computadora.model_dump())
        self.db.add(new_computadora)
        self.db.commit()
        return
    def update_computadora(self, id: int,data: Computadora):
        computadora = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).fist()
        computadora.marca = data.marca
        computadora. modelo = data.modelo
        computadora.color = data.color
        computadora.ram = data.ram
        computadora.almacenamiento = data.almacenamiento
        self.db.commit()
        return
        
    def delate_computadora(self, id: int):
        self.db.query(ComputadoraModel).fliter(ComputadoraModel.id ==id).delate()
        self.db.commit()
        return


