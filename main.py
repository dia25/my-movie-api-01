from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel
from utils.jwt_manager import create_token
from config.database import Session, engine, Base
from middlewares.error_handler import ErrorHandler
from routes.computadoras import computadora_router
from schemas import user

app = FastAPI()
app.title = "Compra y venta de Computadoras"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(computadora_router)

Base.metadata.create_all(bind=engine)
 
# Definición de modelos
class User(BaseModel):
    email: str
    password: str

# Definición de rutas y operaciones 
@app.get('/', tags=['Inicio'])
def message():
    return HTMLResponse('<h1>Venta de Computadoras</h1>')


